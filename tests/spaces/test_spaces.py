import unittest

from unittest.mock import patch, MagicMock
from src.spaces import spaces


class TestSpaces(unittest.TestCase):

    @patch('boto3.client')
    def test_space_set_up(self, patched_class):
        spaces.Spaces()
        assert patched_class.called

    def test_file_name_correclty(self):
        space = spaces.Spaces()
        space.space.Object = MagicMock()
        url = space.upload_file('testing')
        space.space.Object.assert_called_with('austin-testing', 'testing.png', ContentType='image/png')
        assert url is not None
