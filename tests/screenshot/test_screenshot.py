import unittest
from unittest.mock import patch
from src.screenshot import screenshot


class TestScreenShot(unittest.TestCase):

    @patch('subprocess.call')
    def test_upper(self, patched_call):
        screenshot.Screenshot().take_screenshot()
        assert patched_call.called

    @patch('subprocess.call')
    def test_random_name_created(self, patched_call):
        first_screenshot = screenshot.Screenshot()
        second_screenshot = screenshot.Screenshot()

        assert first_screenshot.name != second_screenshot.name


if __name__ == '__main__':
    unittest.main()
