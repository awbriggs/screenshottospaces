import subprocess
import uuid

class Screenshot:

    def __init__(self):
        self.name = str(uuid.uuid4())

    def take_screenshot(self) -> None:
        """
        Saving to a random filename

        :return: None
        """
        subprocess.call(["gnome-screenshot", "-a", "-f", "/tmp/{}.png".format(self.name)])
