from . import screenshot


def main():
    screenshot_whatever = screenshot.Screenshot()
    screenshot_whatever.take_screenshot()


if __name__ == '__main__':
    main()
