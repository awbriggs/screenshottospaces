from requests import post

from src.screenshot import screenshot
from src.spaces import spaces
from src.clipboard import clipboard


def main():
    screen = screenshot.Screenshot()
    screen.take_screenshot()
    space = spaces.Spaces()
    url = space.upload_file(screen.name)
    url = shorten_url(url)
    clipboard.add_to_clipboard(url)


def shorten_url(url: str) -> str:
    return "https://link.awbriggs.com/link/" + \
           post("https://link.awbriggs.com/link", json={"oldUrl": url}).json().get('newUrl')


if __name__ == '__main__':
    main()
