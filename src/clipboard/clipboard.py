from subprocess import Popen, PIPE


def add_to_clipboard(url):
    p = Popen(['xsel', '-bi'], stdin=PIPE)
    p.communicate(input=url.encode())
