import boto3


class Spaces:

    def __init__(self):
        self.space = boto3.client('s3', endpoint_url='https://nyc3.digitaloceanspaces.com')

    def upload_file(self, file_name) -> str:
        """

        :param file_name:
        :return: Signed_url that expires in 3600 seconds
        """
        self.space.upload_file('/tmp/{}.png'.format(file_name), 'austin-testing', '{}.png'.format(file_name), ExtraArgs={'ContentType': 'image/png'})
        return self.space.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': 'austin-testing',
                'Key': file_name + '.png'

            }, ExpiresIn=3600)


